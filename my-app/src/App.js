import './App.css';
import React, { Component } from "react";
import StudentComponent from './components/StudentComponent';
import ProfesorComponent from './components/ProfesorComponent';
import UniversityComponent from './components/UniversityComonent';
import TodosComponent from './components/TodosComponent';
import DropDown from './components/DropDown';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { format } from "date-fns";


class App extends Component {
  state={
    language:{label:"Eng",value:"eng"}
  };
  render() {
    return (
      <Router>
      <div className="navigation">
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/Profesors">Profesors</Link>
            </li>
            <li>
              <Link to="/Universities">Universities</Link>
            </li>
            <li>
              <Link to="/Students">Students</Link>
            </li>
            <li>
              <Link to="/Todos">Todos</Link>
            </li>
            <li style={{paddingLeft:"10px",color:"white", position: "absolute", right:"20px"}}>
              <p>Current date: {format(new Date(), "dd-MM-yyyy")}</p>
            </li>
            <li style={{paddingLeft:"10px",color:"white", position: "absolute", width:"100px", right:"220px"}}>
            <DropDown  
            value={this.state.language}
            onChange={event=>this.setState({language:event})}
            options={[{label:"Eng",value:"eng"},{label:"Ser",value:"ser"}]}
            required={true}
          />
            </li>
          </ul>
        <Switch>
          <Route path="/Students">
            <StudentComponent />
          </Route>
          <Route path="/Profesors">
            <ProfesorComponent />
          </Route>
          <Route path="/Universities">
            <UniversityComponent />
          </Route>
          <Route path="/Todos">
            <TodosComponent />
          </Route>
          <Route path="/">
            <StudentComponent />
          </Route>
        </Switch>
      </div>
    </Router>
    );
  }
}

export default App;
