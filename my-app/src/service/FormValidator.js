const FormValidator = {
    emailPattern:"^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$",
    validate:function(form){
       let data={}; 
       let errors={};
        if(!form)
          return  {valid:true,data,errors}; 
        for(let i=0;i<form.current.elements.length;i++){
            let el=form.current[i];
            let value=el.defaultValue;
            data[el.name]=value;
            if(value===undefined || value===null || value.toString().trim().length===0){
              if(el.required){
                errors[el.name]='input is required';
              }
            }
            else{
              if(el.type==="number"){
                if(el.min && Number(value.replace(',','.'))<Number(el.min)){
                  errors[el.name]='value must be greater than '+el.min;
                }else  if(el.max && Number(value.replace(',','.'))>Number(el.max)){
                  errors[el.name]='value must be lower than '+el.max;
                }
              }
              else if([undefined,"text" ,"email" ,"password"].includes(el.type)){
                if(el.maxLength && el.maxLength>0 && value.length>el.maxLength){
                  errors[el.name]='max length is'+el.maxLength;
                }else  if(el.minLength && value.length<el.minLength){
                  errors[el.name]='min length is'+el.minLength;
                }else if(el.pattern && !value.match(el.pattern)){
                  errors[el.name]='incorrect input';
                }else if(el.type==="email" && !value.match(this.emailPattern)){
                    data[el.name]=value;
                }
              }
            }
          }
        return {valid:Object.keys(errors).length===0,data,errors};
    },
    clear:function(form){
      if(!form)
        return  null;
      let returnValue={};  
      for(let i=0;i<form.current.elements.length;i++){
        let el=form.current[i];
        returnValue[el.name]='';
      }
      return returnValue;
    }, 
    validateAllForms:function(){
      let valid=true;
      let errors=[];
      document.forms.forEach(element => {
         const result=this.validate(element);
         valid=valid && result.valid;
         errors.push(result.error)
      });
      return {valid,errors};
    }
}

export default FormValidator;