const FilterDataService = {
    filterData:function(inputData, columnDefinition, regex){
      if (!regex) {
        return inputData;
      }
      const regexWords = regex.toLowerCase().split(' ');
      return inputData.filter(element => {
        return regexWords.every(word => {
          return columnDefinition(element).some(property => property.toString().toLowerCase().includes(word));
        });
      });
    }
}

export default FilterDataService;