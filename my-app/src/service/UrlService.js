
const UrlService={
    getQueryParams:function(){
       const queryString= window.location.search.split('?')[1];
       if(!queryString)
          return {};
          let queryParams={};
          queryString.split("&").forEach(e=>{
              queryParams[e.split("=")[0]]=this.paresStringFromUrl(e.split("=")[1]);
          });
          return queryParams; 
    },
    setQueryParams:function(params){
        window.location=window.location.search.split('?')[0]+"?"+Object.keys(params).map(e=> e+"="+this.formatObjectInUtl(params[e])).join("&");
    },
    formatObjectInUtl:function(object){
       return Array.isArray(object)?object.join(","):object instanceof Date ? object.toLocaleDateString('en-US'): object;
    },
    paresStringFromUrl(string){
        if(string==='true')
          return true;
        if(string==='false')
          return false;
        if(!isNaN(string))  
          return Number(string);
        if(this.isDate(string))
          return new Date(string);
        if(string.includes(','))
          return string.split(',');
        return string;
    },
    isDate :function(date) {
        return !isNaN(new Date(date).getDate());
    }
}
export default UrlService;