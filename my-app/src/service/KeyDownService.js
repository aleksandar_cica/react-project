const KeyDownService = {
    setFocus:function(event,form,defaultFocus){
        if(!form){
            return;
        }
        if(event.key === 'Enter' && event.target.tagName!=="TEXTAREA"){
            const index = Array.prototype.indexOf.call(form, event.target);
            let focusNext=this.focusNext(form,index);
            if(!focusNext && defaultFocus){
              defaultFocus.current.focus();
            }
            event.preventDefault();
        }
    },

    focusNext:function(form,index){
        for(let i=index+1;i<form.elements.length;i++){
            const element=form.elements[i];
            const className=element.className;
            if((!className || !className.includes("not-focusable")) 
                && element.readOnly!==true && element.disabled!==true && element.type!=='checkbox'){
              form.elements[i].focus();
              return true;
            }
        }
        return false;
    },
    firstFocus:function(form){
        for(let i=0;i<form.elements.length;i++){
            const element=form.elements[i];
            if(element.disabled!==true && element.readOnly!==true){
                element.focus();
                break;
            }
        }
    }

}    

export default KeyDownService;