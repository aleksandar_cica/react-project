
import React, { Component } from "react";
import TableForm from './TableForm';
import UniversityForm from './UniversityForm';



class UniversityComponent extends Component {
  state = {
    university: [
       { id: 1, name: 'university1',  town: 'Boston' ,schooling:9500},
       { id: 2, name: 'universit2',  town: 'New York',schooling:15000 },
       { id: 3, name: 'university3',  town: 'LA',schooling:20000 },
       { id: 4, name: 'university4',  town: 'New York',schooling:25000 },
    ],
    numberFormater:new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD'
    }),
    selectedStudent:1
 };

  headerDefintion=()=>{
     return ['Name','Town',"Schooling"];
  }
  rowDefintion=element=>{
    return [element.name,element.town, element.schooling];
  }

  render() {
    return (
      <TableForm 
      data={this.state.university}
      headerDefintion={this.headerDefintion}
      rowDefintion={this.rowDefintion}
      rightClickContent={this.rightClickContent}
      numberFormater={this.state.numberFormater}
      >
      <UniversityForm/>
      </TableForm>
    );
  }
}

export default UniversityComponent;
