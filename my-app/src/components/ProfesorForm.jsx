import React from "react";
import ConfirmDialog from "./ConfirmDialog";
import UniversityForm from './UniversityForm';
import EmailInput from "./EmailInput";
import DropDown from './DropDown';

 class ProfesorForm extends React.Component{
   state={
    university:{},
    menuIsOpen:false,
    cities:[],
    options:[{value:1,label:"university1"},{value:2,label:"university2"},
    {value:3,label:"university3"}]
   };

  confirmClicked=university=>{
    const {options}=this.state;
    options.push({label:university.name,value:-1});
    this.setState(options);
  }

  componentDidUpdate(prevProps){
     if(prevProps.cities!==this.props.cities){
       this.setState({cities:this.props.cities});
     }
  }

  
  render() { 
    let {Name,City,Country,email,university}=this.props.selectedElement || {};
    const {disabled,onChange,formErrors}=this.props;
    const {cities,options}=this.state;
  return (
    <div>
      <div className="form-group">
        <input 
          className="firstFocus"
          disabled={disabled} 
          onChange={onChange}  
          id="Name" 
          name="Name"
          placeholder="Name"
          required
          maxLength="50"
          value={Name || ''}
        />
        <span className="error">{formErrors.Name}</span>
      </div>
      <div className="form-group">
        <EmailInput 
          onChange={onChange}  
          disabled={disabled} 
          value={email} 
          required={true}
        />
        <span className="error">{formErrors.email}</span>
      </div>
      <div>
        <input 
          onChange={onChange}  
          disabled={disabled} 
          value={Country || ''} 
          type="text" 
          name="Country"
          placeholder="Country"
          required
        />
        <span className="error">{formErrors.Country}</span>
      </div>
      <div className="form-group">
         <DropDown 
            placeholder="Select City"
            onChange={event=>  this.props.handleSelectChange(event,'City')} 
            name="City"   
            value={{label:City,value:City}}
            options={cities}
            isDisabled={disabled}
            required={true}
          />
        <span className="error">{formErrors.City}</span>
      </div>
      <div className="form-group row no-gutters">
         <DropDown 
            className="col-11"
            placeholder="Select University"
            onChange={event=>  this.props.handleSelectChange(event,'university')} 
            name="university"   
            value={university}      
            options={options}
            isDisabled={disabled}
            required={true}
          />
        <span className="error">{formErrors.university}</span>
        <ConfirmDialog 
          onConfirm={this.confirmClicked}
          buttonText={"+"} 
          disabled={disabled}
          buttonStyle={{fontSize:"18px", height: '30px'}}
          title={"Add university"}
          >
          <UniversityForm slot="content"/>
        </ConfirmDialog>
      </div>
    </div>
    );
  }
}

export default ProfesorForm;