import React from "react";

class NumberInput extends React.Component{
    state={
      value:0,
      step:"1"
    };
    componentDidMount=()=> { 
        const {value,step,limitDecimals}=this.props;
        this.setState({value})
        if(!step && limitDecimals){
            this.setState({step:1/Math.pow(10,Number(limitDecimals))})
        }else if(step){ 
            this.setState({step})
        }
        this.onChange = this.onChange.bind(this);
    }
    componentDidUpdate(prevProps) {
        if (prevProps.value !== this.props.value) {
          this.setState({value:this.props.value});
        }
    }
    onBlur=()=>{
        const {onChange,name,max,min}=this.props;
        if(typeof onChange !== 'function')
          return;
        const {value}=this.state;
        if(min!==undefined && Number(min)>value){
            onChange({target:{name,value:min,type:"number"}}); 
        } else if(max!==undefined && Number(max)<value){
            onChange({target:{name,value:max,type:"number"}}); 
        } else{
            onChange({target:{name,value:this.state.value,type:"number"}}); 
        }
    }

    onChange=event=>{
      const {onChange,name}=this.props;
       const {limitDecimals}=this.props;
       if(limitDecimals===undefined){ 
          this.setState({target:{value:event.target.value}})
       }else{
          const value=event.target.value.toString(); 
          const length=value.length;
          const dotPosition=value.indexOf('.');
          const comaPosition=value.indexOf(',');
          if(Number(limitDecimals)===0){
              if(dotPosition>-1){ 
                this.setState({value:value.replace('.','')}) 
              }else if(comaPosition>-1){
                this.setState({value:value.replace(',','')}) 
              }else{
                this.setState({value})
              }
              return;
          }
          if((dotPosition>-1 && length>dotPosition+Number(limitDecimals)+1) || 
              (comaPosition>-1 && length>comaPosition+Number(limitDecimals)+1)){
                this.setState({value:value.substr(0,length-1)})
          }else{
            this.setState({value})
          } 
       }
      onChange({target:{name,value:this.state.value}});
    }

   
    render() { 
     const {min,max,required,name,placeholder,disabled,style,readOnly,className}=this.props;
      return (
        <input 
           onChange={this.onChange}
           disabled={disabled} 
           readOnly={readOnly}
           style={style || {}}
           type="number" 
           value={this.state.value}
           min={min} 
           max={max} 
           required={required} 
           name={name} 
           step={this.state.step}
           placeholder={placeholder} 
           className={className}
           onFocus={event=> event.target.select()}  
           onClick={event=> event.target.select()} 
           onBlur={this.onBlur}
           onKeyDown={this.onKeyDown}
        />
      )
    }
}

export default NumberInput;