import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import 'bootstrap/dist/css/bootstrap.min.css';
import BaseForm from './BaseForm';

class ConfirmDialog extends Component {
  constructor(props){
    super(props);
    this.submit=React.createRef();
    this.base=React.createRef();
  }
    state = {
       open:false
    };

   handleClickOpen = () => {
     this.setState({open:true});
   };

   handleClose = () => {
    this.setState({open:false});
    if(typeof this.props.onClose==='function'){
      this.props.onClose();
    }
   };
   handleConfirm=()=>{
        if(typeof this.props.onConfirm === 'function'){       
            if(this.base && this.base.current){
              const validationResult=this.base.current.validate();
              if(validationResult.valid){
                this.props.onConfirm(validationResult.data); 
                this.setState({open:false});
              }
            }else{
              this.props.onConfirm(); 
              this.setState({open:false});
            }
        }else{
          this.setState({open:false});
        }
   }
   findChildBySlotName(children,slotName){
       return children && (Array.isArray(children)?children:[children]).filter(e=>e.props.slot===slotName);
   }
    createContent(children,text){
      const element=this.findChildBySlotName(children,'content');
      return (element && element.length>0 && 
        <BaseForm ref={this.base} formClass={"dialogForm"}  visible={true}>{element[0] }</BaseForm>) || 
        <DialogContentText id="alert-dialog-description">
         {text || 'Are you sure?'}
        </DialogContentText> 
    }
  render() { 
      const {disabled,title,text,buttonStyle,children}=this.props;
    return (
        <div>
        <button className="btn"  style={buttonStyle} disabled={disabled}  onClick={this.handleClickOpen}>
           { this.props.buttonText || 'Open alert dialog'}
        </button>
        <Dialog
            open={this.state.open}
            onClose={this.handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
            <DialogContent>  
              { this.createContent(children,text)}
            </DialogContent>
            <DialogActions>
            <button className="btn" onClick={this.handleClose} color="primary">
               { this.props.Disagree || 'No'}
            </button>
            <button className="btn" onClick={this.handleConfirm} color="primary" autoFocus>
               { this.props.Agree || 'Yes'}
            </button>
            { this.findChildBySlotName(children,'Buttons')}
            </DialogActions>
        </Dialog>
        </div>
        );
    }
}

export default ConfirmDialog;