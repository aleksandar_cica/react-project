
import React, { Component } from "react";
import TableForm from './TableForm';
import ProfesorForm from './ProfesorForm';
import axios from 'axios';
import ls from 'local-storage'

class ProfesorComponent extends Component {
  state = {
    profesors: [],
    cities:[],
    scrollPosition:0
 };
componentDidMount() {
  axios.get('https://www.w3schools.com/angular/customers.php').then(result=>{
    const profesors=result.data.records.map((element,index)=>{
      return {...element,id:index,email:element.Name.replace(" ","")+"@gmail.com"};
    });
    const cities=[...new Set(profesors.map(element=>element.City))].sort().map(e=> {
      return {value:e,label:e}
    });
    this.setState({profesors,cities});
    this.setState({scrollPosition:ls.get('scrollPosition')})
  });
 }

  headerDefintion=()=>{
     return ['Name','City','Country'];
  }
  rowDefintion=element=>{
    return [element.Name,element.City,element.Country];
  }

  rightClickContent(element){
    return <button className="btn btn-success">Some Button</button>;
  }

 

  render() {
    return (
      <TableForm 
      data={this.state.profesors}
      headerDefintion={this.headerDefintion}
      rowDefintion={this.rowDefintion}
      rightClickContent={this.rightClickContent}
      onScrollPositionChanged={scrollTop=> ls.set('scrollPosition', scrollTop)}
      scrollPosition={this.state.scrollPosition}
      >
      <ProfesorForm cities={this.state.cities}/>
      </TableForm>
    );
  }
}

export default ProfesorComponent;
