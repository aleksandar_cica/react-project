import React, { Component } from 'react';
import '../index.css';
import FilterDataService from '../service/FilterDataService';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import {DebounceInput} from 'react-debounce-input';
import Checkbox from 'rc-checkbox';


class MyTable extends Component {
    constructor(props){
        super(props);
        this.table=React.createRef();
        this.scrollDiv=React.createRef();
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }
    state = {
       selectedElement:{},
       activeHeader:-1,
       setScrollPosition:true,
       searchInput: "",
       displayedData:[],
       reverese:[],
       rowsPerPageOptions:[],
       rowsPerPage:0,
       showMenu:false,
       xPos:0,
       yPos:0,
       filterOnBackEnd:false
    };

    componentDidUpdate(prevProps){
      if(prevProps.data!==this.props.data){
        this.setState({displayedData:this.props.data});
      }
    }
    
    componentDidMount() {
      if(this.props.filterOnBackEnd===undefined){
        this.setState({filterOnBackEnd:typeof this.props.onFilter==='function'});
      }
      document.addEventListener('click', this.handleClickOutside, true);
      let rowsPerPageOptions=this.props.rowsPerPageOptions;
      if(!rowsPerPageOptions){
        rowsPerPageOptions=[5, 10, 15, { label: 'All', value: -1 }]
      }else{
        rowsPerPageOptions.push({ label: 'All', value: -1 });
      }
        this.setState({ 
          displayedData :this.props.data,
          reverese:this.props.headerDefintion().map(()=>false),
          rowsPerPageOptions:rowsPerPageOptions,
          rowsPerPage:this.props.rowsPerPage || 10
        });
    }

    componentWillUnmount () {
      document.removeEventListener('click',this.handleClickOutside)
    }
    handleChangePage=event=>{
      
    }
    handleChangeRowsPerPage=event=>{
      this.setState({rowsPerPage:Number(event.target.value)})
    }
    TablePaginationActions=()=>{
      return "-"+this.state.displayedData.length;
    }
    openDialog(event,e){
      if(typeof this.props.rightClickContent==='function'){
        event.preventDefault();
        this.setState({showMenu:true,xPos:event.pageX,yPos:event.pageY,selectedElement:{...e}});
      }
    }
    closeMenu=()=>{
      if(this.state.showMenu){
        setTimeout(()=>{
          this.setState({showMenu:false,selectedElement:{}});
        })
      }
    }
    handleClickOutside(event) {
      if (this.table && this.table.current && !this.table.current.contains(event.target)) {
        this.closeMenu();
      }
  }

  rowClassName(element,index){
    return this.state.selectedElement.id===element.id ? "selectdRow" :index%2===0 ? "evenRow":"";
  }

  onScroll =()=>{
    const {clientHeight,scrollTop,scrollHeight}=this.scrollDiv.current;
    if(typeof this.props.onScrollPositionChanged==='function'){
      this.props.onScrollPositionChanged(scrollTop);
    }
    if (clientHeight + scrollTop >=scrollHeight - 30 && typeof this.props.onScroll=== 'function') {
      this.props.onScroll();
    }
  }

  setScrollPosition(index) {
     const headerHeight = this.table.current.getElementsByTagName('tr')[0].clientHeight;
     const rowHeight = this.table.current.getElementsByTagName('tr')[1].clientHeight;
     this.scrollDiv.current.scrollTop=headerHeight+index*rowHeight;
  }

  onDoubleClick=element=>{
    const {onDoubleClick}=this.props;
    this.table.current.focus();
    if(typeof onDoubleClick==='function'){
      onDoubleClick(element);
    }
  }

  onFocusInput=()=>{
    this.setState({selectedElement:{}})
  }
  onKeyUpInput=event=>{
    if(event.keyCode===40){
      this.table.current.focus();
      this.setState({selectedElement:this.props.data[0] || {}})
    }
  }

   render() { 
    const {showPagination,showFooter,rightClickContent,hideSearch,numberFormater}=this.props;
    const {rowsPerPageOptions,rowsPerPage,showMenu,xPos,yPos,filterOnBackEnd}=this.state;
      return (
        <div onClick={this.closeMenu}>
        {hideSearch ||  <DebounceInput
          placeholder="Filter"
          onFocus={this.onFocusInput}
          value={this.state.searchInput || ""}
          debounceTimeout={filterOnBackEnd?1000:0}
          onKeyUp={this.onKeyUpInput}
          onChange={this.filter} />
        }
        <div className="tableWrapper" ref={this.scrollDiv} onScroll={this.onScroll}>
        <table tabIndex="0"  ref={this.table} 
           onKeyUp={this.handleKeyUp}>
           <thead className="table sticky">
            <tr style={{cursor:"pointer", backgroundColor:"green"}}>
              {this.renderTableHeader()}
            </tr>
           </thead>
           <tbody className="body">
           {this.state.displayedData.map((e,index)=>
           <tr onContextMenu={(event)=>this.openDialog(event,e)}
             className={this.rowClassName(e,index)} 
             onDoubleClick={()=>this.onDoubleClick(e)}
             key={e.id} onClick={event => this.handleClick(e,event)}>{this.renderTableRow(e,index,numberFormater)}
           </tr>
           )}
           </tbody>
       { showFooter &&   <TableFooter>
          <TableRow>
           {showPagination && <TablePagination
              rowsPerPageOptions={rowsPerPageOptions}
              colSpan={3}
              count={this.state.displayedData.length}
              rowsPerPage={rowsPerPage || 10}
              page={0}
              SelectProps={{
                inputProps: { 'aria-label': 'rows per page' },
                native: true,
              }}
              onChangePage={this.handleChangePage}
              onChangeRowsPerPage={this.handleChangeRowsPerPage}
              ActionsComponent={this.TablePaginationActions}
           /> }
          </TableRow>
        </TableFooter>
        }
        </table>
        </div>
          <>
            {showMenu ? (
              <div
                style={{
                  top: yPos+"px",
                  left: xPos+"px",
                  opacity: 0.75,
                  position: "absolute"
                }}
              >
                {rightClickContent(this.state.selectedElement)}
              </div>
            ) : (
              <></>
            )}
          </>
      </div>
      );
   }

  filter=event=>{
    const searchInput = event.target.value;
    if(this.state.filterOnBackEnd){
        this.props.onFilter(searchInput);
    }else{
      const {rowDefintion,data}=this.props;
      const displayedData=FilterDataService.filterData(data,rowDefintion,searchInput);
      this.setState({setScrollPosition:false, searchInput,displayedData});
    }
  }

   handleClick = (element,event) => { 
       this.table.current.focus();
       this.setState({selectedElement:{...element}},);
       if(typeof this.props.selectionChanged === 'function' && !event.ctrlKey){
         this.props.selectionChanged(element);
       }else if(typeof this.props.ctrlClick === 'function' && event.ctrlKey){
         this.props.ctrlClick(element);
       }
    }
  handleKeyUp = event => {
     switch(event.keyCode ) {
        case 13:
           if(typeof this.props.onEnter==='function'){
              this.props.onEnter(this.state.selectedElement);
           }
           break;
        case 38:this.setSelectedElement('Up'); break;
        case 40:this.setSelectedElement('Down'); break;
        default: break;
      }
  }

   renderTableHeader=()=> {
        return this.props.headerDefintion().map((element, index) => {
           return <th className={this.getHeaderClass(index)} onClick={()=>this.sort(index)} key={index}>{element}</th>
        })
    }
    getHeaderClass=(index)=>{
       return index===this.state.activeHeader?(this.state.reverese[index]?"headerSortDown":"headerSortUp"):"";
    }
    renderTableRow = (element,index,numberFormater)=> {
        if(index===(this.state.displayedData || []).length-1 && this.state.setScrollPosition){
            this.scrollDiv.current.scrollTop=this.props.scrollPosition;
        }
        return this.props.rowDefintion(element).map((result, index) => {
            if(result===true || result===false){
            return  <td  key={index}><Checkbox
                checked={result}
                disabled={false}
              /></td>
            }
            const isNumber=!isNaN(result);
            const formatedResult=isNumber && numberFormater && typeof numberFormater.format ==='function' ?
            numberFormater.format(result):result;
            return <td className={isNumber? 'align-right':''} key={index}>{formatedResult}</td>
        })
    }
    setSelectedElement = event =>{
        const {displayedData}=this.state;
        let {selectedElement}=this.state;
        const uniqueProperty=this.props.uniqueProperty || 'id';
        const index=displayedData.findIndex(element=>element[uniqueProperty]===selectedElement[uniqueProperty]);
        if(event==='Up'){
           if(index!==0){
               selectedElement={...displayedData[index-1]};
               this.setScrollPosition(index-1);
           }else{
            selectedElement={...displayedData[this.props.data.length-1]};
            this.setScrollPosition(this.props.data.length-1);
           }
        }else{
            if(index<displayedData.length-1){
                selectedElement={...displayedData[index+1]};
                this.setScrollPosition(index+1);
            }else{
              selectedElement={...displayedData[0]};
              this.setScrollPosition(0);
            }
        }
        if(typeof this.props.selectionChanged === 'function'){
          this.props.selectionChanged(selectedElement);
        }
        this.setState({selectedElement});
    }

    sort=index=>{
      const {rowDefintion}=this.props;
      const {displayedData,reverese}=this.state;
      displayedData.sort((e1,e2)=>{
        return (rowDefintion(e1)[index]>rowDefintion(e2)[index]?1:-1)*(reverese[index]?-1:1);
      });
      reverese[index]=!reverese[index];
      this.setState({activeHeader:index, setScrollPosition:false,displayedData:this.state.displayedData})
    }
}

export default MyTable;