import React, { Component } from 'react';
import FormValidator from '../service/FormValidator';


class BaseForm extends Component {
  constructor(props){
    super(props);
    this.form=React.createRef();
  }

  state={
    element:{},
    formErrors:{}
  };

  validate(){
    const validationResult= FormValidator.validate(this.form);
    this.setState({formErrors:validationResult.errors})
    return validationResult;
  }

  handleChange=event=> {
    const {name,value,checked}=event.target;
    this.setState({element: {...this.state.element,[name]:value || checked} });
  }

    componentDidMount(){
      setTimeout(() => {
        for(let element of document.getElementsByClassName(this.props.formClass)[0].getElementsByClassName("firstFocus")){
          if(element.disabled!==true  && element.readOnly!==true){
           element.focus();
           break;
          }
        }      
      });
    }
   render (){
     const {formClass,selectedElement,edit,handleChange,handleSelectChange,disabled}=this.props
       return (
           <form autoComplete="off" ref={this.form} className={formClass}>
            { React.cloneElement(this.props.children, 
              {
                selectedElement:selectedElement || this.state.element,
                formErrors:this.state.formErrors,
                edit:edit,
                onChange:handleChange || this.handleChange,
                handleSelectChange:handleSelectChange,
                disabled:disabled,
              }
            )}
           </form>
       )
   }
}

export default BaseForm;