import React from "react";
import NumberInput from './NumberInput';

 class UniversityForm extends React.Component{
  render() {  
    let {name,town,schooling}=this.props.selectedElement || {};
    const {disabled,onChange,formErrors}=this.props;

  return (
    <div id="universityForm">
      <div className="form-group">
        <input 
          className="firstFocus"
          disabled={disabled} 
          type="text" 
          id="UniversityName" 
          placeholder="University name"
          name="name"
          required
          maxLength="50"
          onChange={onChange}
          value={name || ''}
        />
        <span className="error">{formErrors.name}</span>
        </div>
        <div className="form-group">
        <input 
          type="text" 
          id="UniversityTown" 
          placeholder="Town"
          name="town"
          disabled={disabled} 
          required
          maxLength="50"
          onChange={onChange}
          value={town || ''}
        />
        <span className="error">{formErrors.town}</span>
      </div>
      <div className="form-group">
      <NumberInput 
        onChange={onChange}  
        disabled={disabled} 
        value={schooling} 
        name="schooling"
        placeholder="schooling"
        min="0"
        max="100000"
        limitDecimals="2"
        required
        />
        <span className="error">{formErrors.schooling}</span>
        </div>
      </div>
    );
  }
}

export default UniversityForm;