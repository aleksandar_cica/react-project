import React from "react";
import Select from 'react-select';

class DropDown extends React.Component{
     state={
         isOpen:false,
         selectedValue:{}
     };

     onChange = event=>{
       if(!this.props.multiple){
         this.setState({isOpen:false});
       }
       this.props.onChange(event);
     }

     componentDidMount() {
    
     }

    render() { 
       const customStyles= {
            control: (provided, state) => ({
              ...provided,
              background: '#fff',
              borderColor: '#9e9e9e',
              minHeight: '30px',
              height: '30px',
              boxShadow: state.isFocused ? null : null,
            }),
        
            valueContainer: (provided, state) => ({
              ...provided,
              height: '28px',
              padding: '0 6px'
            }),
        
            input: (provided, state) => ({
              ...provided,
              margin: '0px',
            }),
            indicatorSeparator: state => ({
              display: 'none',
            }),
            indicatorsContainer: (provided, state) => ({
              ...provided,
              height: '28px',
            }),
            option: (styles, { data, isDisabled, isFocused, isSelected }) => {
                return {
                  ...styles,
                  fontSize: '10px',
                  textAlign: 'left',
                  width: 'auto',
                  padding : "3px 3px 3px 3px"
                }
            }, 
          };
     const {options,name,placeholder,className,disabled,required,value}=this.props;
      return (
        <Select 
         menuIsOpen={this.state.isOpen}
         onFocus={()=> this.setState({isOpen:true})}
         onBlur={()=> this.setState({isOpen:false})}
         styles ={customStyles} 
         placeholder={placeholder || 'Search'} 
         className={className}
         options={options} 
         name={name}
         onChange={this.onChange}  
         value={value}      
         isDisabled={disabled}
         required={required}
         isClearable={!required}
         />
      )
    }
}

export default DropDown;