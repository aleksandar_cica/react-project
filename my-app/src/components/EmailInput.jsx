import React from "react";

class EmailInput extends React.Component{
    render() { 
     const {value,required,name,placeholder,disabled,style,onChange,readOnly,className}=this.props;
      return (
        <input 
           onChange={onChange}
           disabled={disabled} 
           style={style || {}}
           readOnly={readOnly}
           type="email" 
           className={className}
           value={value || ''}
           required={required} 
           name={name || 'email'} 
           placeholder={placeholder || "Email"} 
           pattern={'^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'}
        />
      )
    }
}

export default EmailInput;