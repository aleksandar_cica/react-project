
import React, { Component } from "react";
import TableForm from './TableForm';
import StudentForm from './StudentForm';
import axios from 'axios';


class TodosComponent extends Component {
  componentDidMount(){
    axios.get('https://jsonplaceholder.typicode.com/todos').then(result=>{
      this.setState({todos:result.data || []})
    });
  }
  state = {
    todos: [],
 }

 deleteById(id){
  axios.delete('https://jsonplaceholder.typicode.com/todos/delete/'+id).then(result=>{
    console.log(result)
   // this.setState({todos:result.data || []})
  });
 }
   
  headerDefintion=()=>{
     return ['Title','Completed'];
  }
  rowDefintion=element=>{
    return [element.title,element.completed];
  }

  rightClickContent=element=>{
    return <button onClick={()=>this.deleteById(element.id)} className="btn btn-danger">Some Button</button>;
  }

  render() {
    return (
      <TableForm 
      data={this.state.todos}
      headerDefintion={this.headerDefintion}
      rowDefintion={this.rowDefintion}
      rightClickContent={this.rightClickContent}
      >
      <StudentForm/>
      </TableForm>
    );
  }
}

export default TodosComponent;
