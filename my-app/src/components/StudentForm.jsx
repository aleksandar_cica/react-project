import React from "react";
import NumberInput from './NumberInput';
import EmailInput from "./EmailInput";
import Checkbox from 'rc-checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
 class StudentForm extends React.Component{

  render() { 
    let {name,email,avarageMark,scholarship}=this.props.selectedElement || {};
    const {disabled,onChange,formErrors,edit}=this.props;
  return (
    <div> 
      <div className="form-group row">
        <input 
          className="firstFocus"
          disabled={disabled}
          readOnly={edit} 
          onChange={onChange}  
          type="text" 
          id="name" 
          name="name"
          placeholder="Name"
          required
          maxLength="50"
          value={name || ''}
        />
        <span className="error">{formErrors.name}</span>
      </div>
        <FormControlLabel
          control={
          <Checkbox onChange={onChange} 
          checked={scholarship} name={'scholarship'}/>
          }
          label={'Scholarship'}
          disabled={disabled}
        />
      <div className="form-group">
        <EmailInput 
          onChange={onChange} 
          className="firstFocus" 
          disabled={disabled} 
          value={email} 
          required={true}
        />
        <span className="error">{formErrors.email}</span>
      </div>
      <div className="form-group">
        <NumberInput 
          onChange={onChange}  
          disabled={disabled} 
          value={avarageMark} 
          name="avarageMark"
          placeholder="Avarage Mark"
          min="6"
          max="10"
          limitDecimals="2"
        />
        <span className="error">{formErrors.avarageMark}</span>
      </div>
    </div>
    );
  }
}


export default StudentForm;