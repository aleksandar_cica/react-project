
import React from 'react'
import CrudForm from './CrudForm';
import MyTable from './MyTable';


class TableForm extends React.Component {
  state = {
    selectedElement:{}
 };

 componentDidMount(){
   document.title=this.props.header || window.document.location.pathname.substr(1);
 }

 getHeader=()=>{
  return this.props.header || window.document.location.pathname.substr(1);
 }

  selectionChanged=selectedElement=>{
     this.setState({selectedElement:{...selectedElement}});
  }


  delete=()=>{
    const {data}=this.props;
    const index=data.findIndex(e=> e.id===this.state.selectedElement.id);
    if(index>-1){
      data.splice(index,1);
    }
    this.setState({data});
  }

  clean=()=>{
    this.setState({selectedElement:{}});
  }
  submitNew=()=>{
   this.props.data.push(this.state.selectedElement);
  }
  submitEdied=()=>{
    const {data}=this.props;
    const index=data.findIndex(e=> e.id===this.state.selectedElement.id);
    if(index>-1){
      data[index]={...this.state.selectedElement};
    }
    this.setState({data});
  }

  handleSelectChange=(event,propertyName)=>{
    this.setState({selectedElement: {...this.state.selectedElement,[propertyName]:event && event.value} });
  }


  handleChange=event=>{
    const {name,value,checked}=event.target;
    this.setState({selectedElement: {...this.state.selectedElement,[name]:value || checked} });
  }

  render() {
    const {data,onEnter,onDoubleClick,ctrlClick,headerDefintion,rowDefintion,rightClickContent,hideSearch,header}=this.props;
    return (
      <div className="wrapper-items-main">
         <h4 style={{textTransform: 'uppercase'}}>{header || window.document.location.pathname.substr(1)}</h4> 
      <div className="wrapper-with-filter">
          <MyTable
          data={data} 
          onEnter={onEnter}
          headerDefintion={headerDefintion}
          rowDefintion={rowDefintion}
          selectionChanged={this.selectionChanged}
          ctrlClick={ctrlClick}
          onDoubleClick={onDoubleClick}
          rightClickContent={rightClickContent}
          hideSearch={hideSearch}
          onFilter={this.props.onFilter}
          numberFormater={this.props.numberFormater}
          dateFormater={this.props.dateFormater}
          scrollPosition={this.props.scrollPosition}
          onScrollPositionChanged={this.props.onScrollPositionChanged}
          />
        <div>
          <CrudForm 
          clean={this.clean} 
          onDelete={this.delete} 
          submitNew={this.submitNew}
          submitEdied={this.submitEdied}
          selectedElement={this.state.selectedElement} 
          handleChange={this.handleChange}
          handleSelectChange={this.handleSelectChange}
          >
            {this.props.children}
          </CrudForm>
        </div>
      </div>
      </div>
    );
  }
}

export default TableForm;
