
import React, { Component } from "react";
import TableForm from './TableForm';
import StudentForm from './StudentForm';
import UrlService from '../service/UrlService';


class StudentComponent extends Component {
  state = {
    students: [
       { id: 1, name: 'Wasif', scholarship: false, email: 'wasif@email.com', avarageMark:8.12 },
       { id: 2, name: 'Ali', scholarship: false, email: 'ali@email.com',avarageMark:7.6 },
       { id: 3, name: 'Saad', scholarship: false, email: 'saad@email.com',avarageMark:8.3 },
       { id: 4, name: 'Asad', scholarship: true, email: 'asad@email.com',avarageMark:9 },
       { id: 5, name: 'Wasif', scholarship: false, email: 'wasif@email.com', avarageMark:8.12 },
       { id: 6, name: 'Ali', scholarship: false, email: 'ali@email.com',avarageMark:7.6 },
       { id: 7, name: 'Saad', scholarship: false, email: 'saad@email.com',avarageMark:8.3 },
       { id: 8, name: 'Asad', scholarship: true, email: 'asad@email.com',avarageMark:9 },
       { id: 9, name: 'Wasif', scholarship: false, email: 'wasif@email.com', avarageMark:8.12 },
       { id: 10, name: 'Ali', scholarship: false, email: 'ali@email.com',avarageMark:7.6 },
       { id: 11, name: 'Saad', scholarship: false, email: 'saad@email.com',avarageMark:8.3 },
       { id: 12, name: 'Asad', scholarship: false, email: 'asad@email.com',avarageMark:9 },
       { id: 13, name: 'Saad', scholarship: true, email: 'saad@email.com',avarageMark:8.3 },
       { id: 14, name: 'Asad', scholarship: false, email: 'asad@email.com',avarageMark:9 },
       { id: 15, name: 'Wasif', scholarship: false, email: 'wasif@email.com', avarageMark:8.12 },
       { id: 16, name: 'Ali', scholarship: false, email: 'ali@email.com',avarageMark:7.6 },
       { id: 17, name: 'Saad', scholarship: false, email: 'saad@email.com',avarageMark:8.3 },
       { id: 18, name: 'Asad', scholarship: false, email: 'asad@email.com',avarageMark:9 },
       { id: 20, name: 'Ali', scholarship: false, email: 'ali@email.com',avarageMark:7.6 },
       { id: 21, name: 'Saad', scholarship: false, email: 'saad@email.com',avarageMark:8.3 },
       { id: 22, name: 'Asad', scholarship: false, email: 'asad@email.com',avarageMark:9 },
       { id: 23, name: 'Saad', scholarship: false, email: 'saad@email.com',avarageMark:8.3 },
       { id: 24, name: 'Asad', scholarship: true, email: 'asad@email.com',avarageMark:9 },
       { id: 25, name: 'Wasif', scholarship: false, email: 'wasif@email.com', avarageMark:8.12 },
       { id: 26, name: 'Ali', scholarship: false, email: 'ali@email.com',avarageMark:7.6 },
       { id: 27, name: 'Saad', scholarship: true, email: 'saad@email.com',avarageMark:8.3 },
       { id: 28, name: 'Asad', scholarship: false, email: 'asad@email.com',avarageMark:9 },
    ],
    selectedStudent:1
 }
   
  headerDefintion=()=>{
     return ['Name','Email','Scholarship','Average Mark'];
  }
  rowDefintion=element=>{
    return [element.name,element.email,element.scholarship,element.avarageMark.toFixed(2)];
  }

  rightClickContent=element=>{
    return <button onClick={()=>UrlService.setQueryParams({id:element.id})} className="btn btn-danger">Some Button</button>;
  }

  mapSelect(value,label){
    return {value,label}
  }
  setSelect(event){
    this.setState({selectedStudent:event});
  }

  render() {
    return (
      <TableForm 
      data={this.state.students}
      headerDefintion={this.headerDefintion}
      rowDefintion={this.rowDefintion}
      rightClickContent={this.rightClickContent}
      header={"Students"}
      >
      <StudentForm/>
      </TableForm>
    );
  }
}

export default StudentComponent;
