import React, { Component } from 'react';
import ConfirmDialog from './ConfirmDialog';
import KeyDownService from '../service/KeyDownService';
import BaseForm from './BaseForm';

class CrudForm extends Component {
    constructor(props){
        super(props);
        this.baseForm=React.createRef();
        this.submitButton=React.createRef();
    }
    state = {
       open:false,
       createClicked:false,
       editClicked:false,
       showDeleteButton:false,
       showCreateButton:false,
       showUpdateButton:false,
       message:"",
       messageType:"success",
       state:false,
       formErrors:{}
    };
    componentDidMount=()=> { 
     const {submitNew,submitEdied,onDelete}=this.props;
      this.setState({
        showDeleteButton:typeof onDelete ==='function',
        showCreateButton:typeof submitNew ==='function',
        showUpdateButton:typeof submitEdied ==='function'
      })
    }
    add=()=>{
      this.setState({createClicked:true,formErrors:{}});
      this.focusFirstElement();
      this.props.clean();
    }
    edit=()=>{
      this.focusFirstElement();
      this.setState({editClicked:true,formErrors:{}});
    }

    focusFirstElement=()=>{
      const elements= document.getElementsByClassName("firstFocus");
      if(!elements){
        return;
      }  
        setTimeout(()=>{
          for(let element of elements){
            if(element.disabled!==true && element.readOnly!==true){
              element.focus();
              break;
            }
          }
        })
    }

    submit=()=>{
      if(!this.validate())
        return;
      if(this.state.createClicked){
        this.props.submitNew();
      }else{
        this.props.submitEdied();
      }
      this.setState({state:true,message:this.createClicked?"Created":"Updated"});
      setTimeout(() => {
        this.setState({state:false});
      },2000);
      this.cancel();
    }
    cancel=()=>{
      this.setState({editClicked:false,createClicked:false,selectedElement:{},formErrors:{}});
      this.props.clean();
    }
    delete=()=>{
      this.props.onDelete();
      this.props.clean();
      this.setState({state:true,message:"Deleted"});
      setTimeout(() => {
        this.setState({state:false});
      },2000);
    }

    componentDidUpdate(prevProps) {
      if (prevProps.selectedElement.id !== this.props.selectedElement.id
      && Object.keys(this.props.selectedElement).length) {
        this.setState({createClicked:false, editClicked:false, formErrors:{}});
      }
    }
  
  validate(){
    const validation=this.baseForm.current.validate();
    return validation.valid;
  }

  handleKeydown = (event) => {
    KeyDownService.setFocus(event,event.target.form,this.submitButton);
  }

  render() { 
      const {createClicked,editClicked,
        showDeleteButton, showCreateButton,showUpdateButton}=this.state;
      const disabled=!this.props.selectedElement || Object.keys(this.props.selectedElement).length===0;
      const disabledForm=!createClicked && !editClicked;
    return (
      <div onKeyDown={this.handleKeydown} className="ml-4">
        
          <BaseForm ref={this.baseForm}
          disabled={disabledForm} 
          edit={editClicked} 
          selectedElement={this.props.selectedElement}
          handleChange={this.props.handleChange}
          handleSelectChange={this.props.handleSelectChange}
          formClass={"baseForm"}
          >
          {this.props.children}
          </BaseForm>
     
          { !createClicked && !editClicked &&  
            <div style={{paddingTop:"20px"}} className="d-flex justify-content-center">
             {showCreateButton && <button className="mr-1 btn " onClick={this.add}>Add</button>}
             {showUpdateButton && <button className="mr-1 btn" disabled={disabled}  onClick={this.edit}>Update</button>}
             {showDeleteButton && <ConfirmDialog buttonStyle={{backgroundColor:"white", color:"red"}}  
             onConfirm={this.delete} disabled={disabled}  buttonText='Delete'/>}
            </div>
          }
          {(createClicked || editClicked) && 
            <div style={{paddingTop:"20px"}} className="d-flex justify-content-center">
              <button className="mr-1 btn" onClick={this.cancel} variant="outlined">Cancel</button>
              <button className="btn" ref={this.submitButton} onClick={this.submit} variant="outlined" color="primary">Submit</button>  
            </div>
          }
      </div>
    );
  }
}

export default CrudForm;

